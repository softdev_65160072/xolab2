/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.xolab2;

import java.util.Scanner;

/**
 *
 * @author sTCFILMs
 */
public class XOLAB2 {
    static char table[][] = {{'-','-', '-'},{'-', '-' ,'-'},{'-', '-','-'}};
    static char turn = 'X';
    static int row,col;
    static String ans = " ";

    public static void main(String[] args) {
        printWelcome();
        while(true){
        printTable();
        printTurn();
        inputRowcol(); 
        if(isWin()){
            printTable();
            printWin();
            break;
        }else if(CheckDraw()){
            printTable();
            printDraw();
            break;
        } 
        switchPlayer(); 
    }
        printRestart();
}
    private static void printWelcome() {
        System.out.println("Welcom To OX Game");
    }

    private static void printTable() {
        for(int i =0;i<3;i++ ){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
            }
            System.out.println("");
        }
    }

    private static void printTurn() {
         System.out.println("Turn "+turn);
    }

    private static void inputRowcol() {
       Scanner kb = new Scanner(System.in);
       while(true){
        System.out.print("Please input row,col :");
        row = kb.nextInt();
        col = kb.nextInt();
        if(table[row-1][col-1]=='-'){
            table[row-1][col-1] = turn;  
            break;
        } 
    }
}
   
    private static void switchPlayer() {
        if(turn == 'X'){
            turn = 'O';
        }else{
            turn = 'X';
        }
    }

    private static boolean isWin() {
        if(CheckRow()){
            return true;
        }if(CheckCol()){
            return true;
        }if(CheckDownleft()){
            return true;
        }if(CheckDownright()){
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println(turn+" Win!!!");
    }
    private static boolean CheckRow() {
        for(int i =0;i<3;i++){
            if(table[row-1][i]!=turn){
            return false;
            }
        }
        return true; 
    }

    private static boolean CheckCol() {
        for(int j =0;j<3;j++){
            if(table[j][col-1]!=turn){
            return false;
            }
        }
        return true;
    }

    private static boolean CheckDownleft() {
        for(int i=0 ;i<3;i++){
            if(table[i][i]!=turn){
                return false;
            }
        }
        return true;
    }

    private static boolean CheckDownright() {
        for(int i=0 ;i<3;i++){
            if(table[i][2-i]!=turn){
                return false;
            }
        }
        return true;
    }

    private static boolean CheckDraw() {
          for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }  

    private static void printDraw() {
        System.out.println("Draw");
    }

 
    private static void printRestart() {
        System.out.println("Restart(YES/NO)");
        Scanner kb = new Scanner(System.in);
        ans = kb.next();
        if(ans.equals("YES")){
            for(int i = 0;i<3;i++){
                for(int j =0;j<3;j++){
                    table[i][j] = '-';
       
                }
            
            }
            printWelcome();
        while(true){
        printTable();
        printTurn();
        inputRowcol(); 
        if(isWin()){
            printTable();
            printWin();
            break;
        }else if(CheckDraw()){
            printTable();
            printDraw();
            break;
        } 
        switchPlayer(); 
    }
        printRestart();
        }
    }
}
